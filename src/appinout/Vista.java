/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appinout;

import java.awt.Robot;

/**
 *
 * @author cecla
 */
public class Vista {
    private String screen[];
    private String titulo = "Adivina si puedes";
    private String instrucciones = "Tienes que ingresar un número dentro de los valores posibles y adivinar";
    private String viditas = "3";
    private String record = "3";
    private String valoresPosibles = "1:20%, 2:25%, 3:20%";
    private String pistas = "Ingresar un número menor";
    private String ingresarValor = "Ingresar Valor: ";
    private Datos param;
   
        
     Vista(){
        screen = new String[7];
    }
    
    Vista(Datos datos){
        screen = new String[7];
        param = datos;
    }
    
    // borrar toda la pantalla
    public void borrarPantalla() {
        try {
            java.awt.Robot pressbot = new java.awt.Robot();
            pressbot.keyPress(17); // Holds CTRL key.
            pressbot.keyPress(76); // Holds L key.
            pressbot.keyRelease(17); // Releases CTRL key.
            pressbot.keyRelease(76); // Releases L key.
            java.util.concurrent.TimeUnit.MILLISECONDS.sleep(100);
        } 
        catch (Exception e){
            System.out.println(e);
        }
    }
    int i = 0;
   
    public void pantallaInicio() {
        screen[0] = titulo;
        screen[1] = instrucciones;
    }
    
    public void vidasRestantes() {
        screen[2] = Integer.toString(param.intentos);
    }
    
    public void suRecord() {
        screen[3] = record;
    }
    
    public void valoresPos() {
        screen[4] = valoresPosibles;
    }
    
    public void pistas() {
        screen[5] = pistas;
    }
            
    public void valor() {
        screen[6] = ingresarValor;         
   }
   
   public void ganador() {
       borrarPantalla();
       refrescarPantalla();
       System.out.println("¡Ganaste guachin!");
   }
   public void perdedor() {
       borrarPantalla();
       refrescarPantalla();
       System.out.println("Erraste");
   }
   public void mayor() {
       System.out.println("El número es mayor");
   }
   public void menor() {
       System.out.println("El número es menor");
   }
    public void refrescarPantalla() {
        for (i = 0 ; i < screen.length ; i++){
            pantallaInicio();
            vidasRestantes();
            suRecord();
            valoresPos();
            pistas();
            valor();
            System.out.println(screen[i]);
        }
        

        
}    
}
